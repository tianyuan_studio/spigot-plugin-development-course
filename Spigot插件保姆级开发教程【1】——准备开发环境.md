# Minecraft我的世界Spigot插件保姆级开发教程【1】——准备开发环境

<font color=gray size=3>更新时间：2020.5.11</font>

本教程以**1.15.2 Spigot**服务端为例。

都0202年了，你还在MCBBS苦苦搜寻称心插件，或者担心插件有后门，或者好不容易找到的插件却作者弃坑，或者因某个插件的某个不完备的功能而失望长叹，或者......

### Spigot插件的优势

我们暂且抛开各个服务端核心之间的性能比较，单纯从spigot插件的优势来讲。首先，spigot能运行在spigot为核心的服务端上，包括spigot，Paper，catserver等。然而Craftbukkit(水桶端)的bukkit插件，Paper端的paper插件，sponge端的sponge插件均只能运行在自己的服务端核心上。所以，spigot插件以其服务端兼容性的优势（可运行在多种服务端核心上）和版本兼容性（一个插件支持多个游戏版本）的优势胜出。目前比较优秀的spigot插件有：EssentialX（基础插件）、Multiverse Core（多世界插件）、Vault（经济前置插件）、GroupManager（权限插件）、Residence（领地插件）等。怎么样，是不是很多都耳熟能详啊！是不是心动了啊！当然，如果编写了spigot插件，并且对性能有极致追求的玩家来说，在paper上运行你写的插件，那岂不是嗨皮到飞起？

### 安装开发Spigot插件所需要的环境

#### 安装集成开发环境(IDEA)与JDK

代码编写自然少不了编写代码的工具，这里介绍一个特别特别好用，在IDE里堪称航空母舰级别的存在。这就是——IntelliJ IDEA。【备注：正式版需要付费，请使用教育版或者自行寻找方法激活】

安装环节过于简单，一直点击下一步，按需调整即可。

**IntelliJ IDEA** 下载地址：[传送门](https://www.jetbrains.com/idea/download/#section=windows)。

**JDK11** 下载地址：[传送门](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

#### 安装Spigot服务端环境

开发spigot插件需要一个spigot的服务端，所以接下来我们就整一个spigot服务端。由于spigot服务端核心官方是不提供的，官方提供的方法就是自己下载BuildTools进行编译构建，那么咱们就按照官方的方法来构建一个spigot服务端核心。

首先打开这个[传送门](https://hub.spigotmc.org/jenkins/job/BuildTools/)，如图一，下载**最终成功**构建下面的**BuildTools.jar**，并将该文件放置在一个文件夹内。

![](./img/1.1.png)

在该文件夹**shift+右键**打开**Powershell**窗口，输入以下指令`java -jar BuildTools.jar --rev 1.15.2`敲下回车，文件就会开始自动下载依赖并进行编译。编译时间可能会比较长，因为下载、编译完后整个文件夹的大小会非常大，需要耐心等待。如果总是编译不成功，请检查自己是否安装了java与JDK，如果还是不行，那就科学上网来解决吧(雾)。

![](./img/1.6.png)

下图为构建成功的截图。

![](./img/1.7.png)

![](./img/1.2.png)

编译完后，文件夹内会多出很多文件（整个文件夹大小在609MB左右），其中包括**spigot-1.15.2.jar（服务端核心文件）**。我们只需要留下**spigot-1.15.2.jar**这个文件，把**其余的全部删掉**即可。现在我们就构建完1.15.2的spigot服务端核心了。

1. 新建一个txt文本文档，打开并输入：

```
@ECHO OFF
java -Xincgc -Xms1g -Xmx2g -jar spigot-1.15.2.jar nogui
pause
```

2. 保存该文档，并将文档重命名为bat后缀；

3. 双击该bat文件即可运行服务器；

4. 输入stop正常关闭服务器。

补充一点，代码的第二行中 “**-Xms1g**”是指最低内存1G，“**-Xmx2g**”是指最高内存2G，自己可以根据情况更改。

第一次开启需要将**eula.txt**里面的**false**改成**true**。

![3](./img/1.3.png)

#### 安装IDEA Minecraft Development插件

我们打开安装好后的IDEA，进入当前界面（图一），点击右下角的Configure==》Plugins按钮。即可打开插件安装面板（图二）。在**MarketPlace**中搜索**Minecraft**关键字找到该插件，点击**Install**就可以完成插件的安装。

![3](./img/1.4.png)

![3](./img/1.5.png)

### 总结

那么这一部分的内容就结束了，是不是感觉很简单呢？正因为Minecraft是Java来编写的，所以入门的门槛相对于其他的游戏就低很多。只要你肯学，稍微有一点点的编程基础，基本上都能学会。当然，你如果能去看Bukkit官方，Spigot官方提供的javadoc来学的话，速度会更快。这一系列教程只能带你入门，具体问题还需要有具体方法来解决。

如果有问题，请在左侧Issue提出，也可以联系面包个人QQ。

文章中所涉及资源如果无法获取，请使用面包准备的[传送门](https://pan.baidu.com/s/1qvyX2qqqKAdcID-DwgZzuw)。提取码：f7gr。

> 未经作者允许，请勿转载文章。