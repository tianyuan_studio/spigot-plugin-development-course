# Minecraft我的世界Spigot插件保姆级开发教程【2】——创建项目&加载通知

<font color=gray size=3>更新时间：2020.5.12</font>

### 创建项目

打开IDEA，点击**Create New Project**。

![2.1](./img/2.1.png)

我们在左侧栏选择**Minecraft**，右边勾选**Spigot Plugin**，然后点击**Next**。

![2.2](./img/2.2.png)

按照样例填写好三个字段。

![2.2](./img/2.3.png)

按照样例填写好字段，**Optional Settings**里面的内容为**选填**(可以留空不填)。填完后，点击**Next**。

![2.2](./img/2.4.png)

按照提示填写好**Project name**（工程名称）、**Project location**（工程地址）。然后点击**Finish**。IDEA就会自动为你配置插件开发所需要的环境，该过程时间可能比较**长**。解决方法：科学上网(雾)。

![2.2](./img/2.5.png)

至此，Spigot插件项目算是构建完毕了。

### 文件目录与代码介绍

工程构建完成后，在IDEA的左栏会一系列的文件。我们进入src==>java==>cn.cupbread.spigotplugincourse(之前创建项目的时候输入的groupID.项目ID)==>SpigotPluginCourse(插件的主类)。

- **代码**一般放在**跟插件主类同级的目录**，**资源以及其他文件**一般放在**resource**目录。

- 我们进入主类后会发现有两个函数，一个叫做**onEnable()**，一个叫做**onDisable()**。两个函数分别对应插件被**载入的时候**，所要执行的操作和插件被**关闭或载出时候**的操作。

- 右边有一个名字为Maven的工具栏。因为咱们这个Spigot使用的是Maven进行项目依赖管理，所以就会有这个工具栏。那么具体的作用是什么呢？我们只用记住两个按钮就可以了。一个是**Lifecycle**（生命周期）内的**clean**，一个是生命周期内的**package**。由于编译打包的时候会生成多余的文件，为了防止多余的文件影响下一次的打包，最好进行一下**clean**操作。**package**顾名思义，就是插件**打包**的意思。

![2.2](./img/2.6.png)

### 制作插件加载/卸载通知

我们在**onEnable()**函数内写入内容【加载插件】

```java
@Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("=========Spigot教学插件============");
        System.out.println("插件加载成功");
        System.out.println("=========Spigot教学插件============");
    }
```

我们在**onDisable()**函数内写入内容【加载插件】

```java
@Override
    public void onDisable() {
        // Plugin shutdown logic
        System.out.println("=========Spigot教学插件============");
        System.out.println("插件卸载成功");
        System.out.println("=========Spigot教学插件============");
    }
```

> **System.out.println("这里填写要打印的内容"); **这个函数的用途就是在控制台打印内容。

然后我们在右边的工具栏双击**package**指令，让IDEA自动执行打包。打包完成后，我们会在左边的文件目录看到多出来了一个名字为target的文件夹。

我们进入该文件夹，文件夹里面的**SpigotPluginCourse-1.0-SNAPSHOT.jar** 就是我们打包出来的插件了。

注意：**original**开头的文件不是打包完成的插件，咱们的插件是没有**original**开头的jar文件。

![2.2](./img/2.7.png)

我们直接选中这个文件，然后**Ctrl+C**复制。然后到之前准备好的1.15.2 spigot服务端下的**plugins**文件夹目录下**Ctrl+V**粘贴。这样，我们的插件就正式装在服务端里啦！我们回到服务端根目录下，双击**启动.bat**文件来启动服务器。

如果一切顺利的话，我们就可以看到我们之前写的代码在控制台打印的日志啦！

![2.2](./img/2.8.png)

同样，我们在控制台内输入**stop**关闭服务器，在服务器关闭后，也能看到插件**卸载**时所执行的**输出函数**。

![2.2](./img/2.9.png)

### 总结

那么本Part的内容就到此为止啦！有没有觉得特别特别简单呢？当然，这个加载卸载的提示也有很多玩法，例如：

![2.2](./img/2.10.png)

如果有问题，请在左侧Issue提出，也可以联系面包个人QQ。

本Part所涉及所有代码均能在[面包房小黑屋代码仓库](http://gitlab.hoh.ink/hoh/spigot-plugin-development-code)内找到。

> 未经作者允许，请勿转载文章。